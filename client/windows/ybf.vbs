'/+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\'
'|  一板斧服务器监控客户端Windows版本 for Windows Server 2003    |'
'|            Powered by YIBANFU @ 2013~2014                     |'
'|         http://yibanfu.com     QQ Group:162616448             |'
'\+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++/'


'''''''''''''''''''首次使用请根据实际需要修改参数''''''''''''''''

CONFIG_USERID          = "test@yibanfu.com"         ' yibanfu.com官方网站注册ID，即Email
CONFIG_CLIENTUID       = "9527"                     ' 用于区别不同主机的编号，可自定义
CONFIG_CIPHERKEY       = "0264ed0cb922792f"         ' 从服务端后台获取的16位或32位的字符串
CONFIG_RESOLVE_TIMEOUT = 5000                       ' HTTP解析超时 ms
CONFIG_CONNECT_TIMEOUT = 5000                       ' HTTP连接超时 ms
CONFIG_SEND_TIMEOUT    = 5000                       ' HTTP发送超时 ms
CONFIG_RECEIVE_TIMEOUT = 5000                       ' HTTP接受超时 ms


''''''''''''''''创建计划任务请使用以下命令'''''''''''''''''''''''
' 手工创建计划任务：每天0点启动，每5分钟执行一次，直到2099年，永远不停
'E:\>SCHTASKS /Create /SC DAILY /DU 23:56 /RI 5 /TN YIBANFU /TR "wscript.exe E:\ybf.vbs run" /ST 00:00 /ED 2099/01/01 /NP
' 手工删除计划任务：
'E:\>SCHTASKS /DELETE /TN YIBANFU
'
'更多帮助信息，请双击脚本文件获得，或访问http://yibanfu.com
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''



''''''''''''''''''''''''本脚本将收集以下信息'''''''''''''''''''''
' -系统各分区大小及当前使用空间
' -主机名、版本、启动时间等操作系统基本信息
' -可用内存和内存使用率
' -CPU使用率
' -网络端口监听状况
' -活动网卡的MAC地址
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''






























'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'                      请勿修改以下代码                         '
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

CONFIG_CLIENTTYPE      = "WINDOWS/VBS-LIGHT"         ' 客户端类型
CONFIG_RELEASE_VERSION = "1.0"                       ' 发布版本号
CONFIG_DEBUG_VERSION   = "0.0.10"                    ' 调试版本号
CONFIG_SERVERURL       = "https://api.yibanfu.com/"  ' 传送门
CONFIG_APPSECKEY       = "12222222222222112"         ' 保留

If WScript.Arguments.Count = 0 Then
    ShowHelp()     ' 双击脚本文件后，直接显式帮助信息
Else
	If getScriptMode() = "wscript.exe" Then
		ChangeRunMode()
	Else
		If     LCase(WScript.Arguments.Item(0)) = "test"      Then
	        ShowTest()
	    ElseIf LCase(WScript.Arguments.Item(0)) = "run"       Then
	        DoMainJob()
	    ElseIf LCase(WScript.Arguments.Item(0)) = "debug"     Then
	        getOSVersionBigNumber()
	    ElseIf LCase(WScript.Arguments.Item(0)) = "install"   Then
	        Install()
	    ElseIf LCase(WScript.Arguments.Item(0)) = "uninstall" Then
	        Uninstall()
	    Else
	    	ShowHelp()
		End If
	End If
End If
Wscript.Quit











'''''''''''''''''''''''已定义的函数I'''''''''''''''''''''''''''''
Function getScriptMode
	getScriptMode = LCase(Right(WScript.FullName, 11))
End Function

Function ChangeRunMode
    ''' 强制以CSCRIPT作为宿主程序运行, 屏蔽黑窗口
    Dim ws
    Set ws = CreateObject("WScript.Shell")
    host   = WScript.FullName
    If LCase(Right(host, 11)) = "wscript.exe" Then
    	strArgs = ""
    	If WScript.Arguments.Count > 0 Then
    	    strArgs = WScript.Arguments.Item(0) '宿主改变，但参数保留
    	End If
    	strCmd = "cmd.exe /c cscript """ & WScript.ScriptFullName & """ " & WScript.Arguments.Item(0)
        ws.run strCmd, 0
        WScript.Quit
    End If
    
    Set ws = Nothing
End Function

Function DoMainJob
    strResp = getHTTPContent(CONFIG_SERVERURL, getPostData())
    HandleHTTPResponse(strResp)
End Function

Function HandleHTTPResponse(strResp)
    If strResp = "" Then
        strResp = "总觉得哪里出了问题 T_T" & vbCrLf & "要不命令行下运行 cscript ybf.vbs test 试试看？"
    End If
    WScript.Echo strResp
    WriteLog(strResp)
End Function

Function ShowTest
    WScript.Echo "功能模块测试..."
    WScript.Echo getDisk()
    WScript.Echo getActiveMAC()
    WScript.Echo getCPU()
    WScript.Echo getCPUUsage()
    WScript.Echo getCodeSet()
    WScript.Echo getOS()
    WScript.Echo getNetworkListeningStatus()
    WScript.Echo getUpTime()
    WScript.Echo getMemory()
    WScript.Echo "准备提交数据..."
    WScript.Echo "服务器返回数据：" & vbCrLf & getHTTPContent(CONFIG_SERVERURL, getPostData())
    WScript.Echo "如果有问题请联系yibanfu官方."
End Function

Function ShowHelp
    strMsg 		= "-----------------------------------------------------------------------" &_
                  vbCrLf & "安装：cscript.exe ybf.vbs install" &_
                  vbCrLf & "卸载：cscript.exe ybf.vbs uninstall" &_
                  vbCrLf & vbCrLf & "Windows 2003创建计划任务时需要输入当前用户的密码" &_
                  vbCrLf & vbCrLf & "-----------------------------------------------------------------------" &_
                  vbCrLf & "测试:cscript.exe ybf.vbs test" &_
                  vbCrLf & "运行:cscript.exe ybf.vbs run"  &_
                  vbCrLf & "日志:" & getLogFilePath()  &_
                  vbCrLf & vbCrLf & "更多帮助请参考脚本源文件或访问http://yibanfu.com"
    If CONFIG_USERID = "test@yibanfu.com" Then
        strMsg  = "-----------------------------------------------------------------------" &_
                  vbCrLf & "提示：实际部署前请修改代码中CONFIG_USERID字段" & vbCrLf & vbCrLf & strMsg
    End If
    WScript.Echo strMsg
End Function

Function Install
    ''' 安装计划任务 
    If getOSVersionBigNumber() > 5 Then
    	strCmd = "cmd.exe /c SCHTASKS /Create /SC DAILY /DU 23:56 /RI 5 /TN YIBANFU /TR ""wscript.exe '{0}' run"" /ST 00:00 /ED 2099/01/01 /NP"
    Else
    	strCmd = "cmd.exe /c start SCHTASKS /Create /SC DAILY /DU 0023:56 /RI 5 /TN YIBANFU /TR ""wscript.exe \""{0}\"" run"" /ST 00:00 /ED 2099/01/01"
    	WScript.Echo vbCrLf & "!!* 为创建计划任务，稍后请正确输入当前操作系统登录用户的密码" & vbCrLf
    	WScript.Echo vbCrLf & "按下Enter键继续..."
    	WScript.StdIn.Read(1)
    End If   
    strCmd               = StrFormat(strCmd, Array(WScript.ScriptFullName))
    WScript.Echo strCmd
    Set objShell         = CreateObject("Wscript.Shell")
    Set objWshScriptExec = objShell.Exec(strCmd)
    Set objStdOut        = objWshScriptExec.StdOut
    WScript.Echo objStdOut.ReadAll()
    WScript.Echo "命令已经执行."  & vbCrLf
    WScript.Echo "请打开控制面板,手工检查确认计划任务成功部署"& vbCrLf &_
                 "或执行命令 ""SCHTASKS /Query""查看 "& vbCrLf & vbCrLf
    If getOSVersionBigNumber() = 5 Then
    	strCmd               = "cmd.exe /c SCHTASKS /Query /FO LIST"
    	Set objWshScriptExec = objShell.Exec(strCmd)
    	Set objStdOut        = objWshScriptExec.StdOut
    	WScript.Echo objStdOut.ReadAll()
    End If
    WScript.Echo "**计划任务列表中的状态 ""未能启动"" 仅表示该任务尚未运行，" & vbCrLf &_
                 "  下个时间周期会自动运行,请勿担心。"
    Set objShell              = Nothing
    Set objWshScriptExec      = Nothing
    Set objStdOut             = Nothing
End Function

Function Uninstall
    ''' 卸载计划任务
    strCmd               = "cmd.exe /c SCHTASKS /DELETE /TN YIBANFU /F"
    WScript.Echo strCmd
    Set objShell         = CreateObject("Wscript.Shell")
    Set objWshScriptExec = objShell.Exec(strCmd)
    Set objStdOut        = objWshScriptExec.StdOut
    If Err.Number <>0 Then
        WScript.Echo Err.Description
    End If
    WScript.Echo objStdOut.ReadAll
    Set objShell         = Nothing
    Set objWshScriptExec = Nothing
    Set objStdOut        = Nothing
    WScript.Echo "命令已经执行."
End Function













'''''''''''''''''''''''已定义的函数II''''''''''''''''''''''''''''
Function StrFormat(FormatString, Arguments())
    ''' 简单型字符串格式化输出 StrFormat("{0}, {1}", Array(str1, str2))
    Dim Value, CurArgNum
    StrFormat = FormatString
    CurArgNum = 0
    For Each Value In Arguments
        If IsNull(Value) Or IsEmpty(Value) Then
            Value = ""
        End If
        StrFormat = Replace(StrFormat, "{" & CurArgNum & "}", Value)
        CurArgNum = CurArgNum + 1
    Next
End Function

Function FromStringToDate(strDateTime)
	strYear    = Mid(strDateTime, 1,  4)
	strMonth   = Mid(strDateTime, 5,  2)
	strDay     = Mid(strDateTime, 7,  2)
	strHour    = Mid(strDateTime, 9,  2)
	strMinute  = Mid(strDateTime, 11, 2)
	strSecond  = Mid(strDateTime, 13, 2)
	strNewDate = strYear & "-" & strMonth & "-" & strDay & " " & strHour & ":" & strMinute & ":" & strSecond
    FromStringToDate =  CDate(strNewDate)
End Function

Function getDisk
    ''' 获取逻辑磁盘的空间和使用状况
	GetDiskInfo = ""
	Dim objWMIService, colItems, diskDict
    Set objWMIService   = GetObject("Winmgmts:{impersonationLevel=impersonate}!\\.\root\cimv2") ''''''''这个表达式有什么特殊的？
    Set colItems        = objWMIService.ExecQuery("Select * From Win32_LOGICALDISK WHERE DriveType=3")
    Set diskDict        = CreateObject("Scripting.Dictionary")
	For Each objItem In colItems
        On Error Resume Next
        strCaption     = "NONE"
        strCaption     = objItem.Caption
        strDeviceID    = "NONE"
        strDeviceID    = objItem.DeviceID
        strDriveType   = "NONE"
        strDriveType   = objItem.DriveType
        strFreeSpace   = "1"
        strFreeSpace   = objItem.FreeSpace
        strSize        = "1"
        strSize        = objItem.Size
        strUseRate     = "0.0"
        strUseRate     = Round((CDbl(strFreeSpace)/CDbl(strSize)) * 100,2)
        strVolumeName  = "NONE"
        strVolumeName  = objItem.VolumeName
        if Err.Number  = 0 Then
            strDiskInfo  = "{'Caption': '{0}', 'DeviceID' : '{1}', 'DriveType'  : '{2}', 'FreeSpaces' : '{3}'," &_ 
                           " 'Size'   : '{4}', 'UseRate'  : '{5}', 'VolumeName' : '{6}'}"
            strDiskInfo  = StrFormat(strDiskInfo, Array(strCaption, strDeviceID, strDriveType, strFreeSpace, strSize, strUseRate, strVolumeName ))
            diskDict.Add strCaption, strDiskInfo           
        End If
        Err.Clear        
	Next
    strReturn = ""
	i = 0
	While i < diskDict.Count
		If i = 0 Then
			strReturn =               "'" & i &"' : " & diskDict.Items()(i) & ""
		Else
			strReturn = strReturn & ", '" & i &"' : " & diskDict.Items()(i) & ""
		End If
		i = i + 1
	Wend

    Set objWMIService   = Nothing
	Set colItems        = Nothing
	Set diskDict        = Nothing
	getDisk = "{" & strReturn & "}"
End Function

Function getUpTime
    'LastBootUpTime=20140127200257.109999+480
    'LocalDateTime=20140127201506.296000+480
	Dim objWMIService, colItems
	strReturnValue = "{'uptime' : '0'}"
    Set objWMIService = GetObject("winmgmts:\\.\root\CIMV2")
	Set colItems = objWMIService.ExecQuery("SELECT * FROM Win32_OperatingSystem WHERE Primary = TRUE", "WQL", &h10 + &h20)
	For Each objItem In colItems
        On Error Resume Next
        strLastBootUpTime  = "19700101010101.000001+480"
        strLastBootUpTime  = CStr(objItem.LastBootUpTime)
        LastBootUpTime     = FromStringToDate(strLastBootUpTime)
        strLocalDateTime   = "19700101010101.000002+480"
        strLocalDateTime   = CStr(objItem.LocalDateTime)
        LocalDateTime      = FromStringToDate(strLocalDateTime)
        uptimeSecond       = DateDiff("s" , LastBootUpTime, LocalDateTime)
        strReturnValue     = StrFormat("{'uptime' : '{0}'}", Array(uptimeSecond))
    Next
    Set objWMIService   = Nothing
    Set colItems        = Nothing
    getUpTime           = strReturnValue
End Function

Function getOSVersionBigNumber
    Dim objWMIService, colItems
    Set objWMIService = GetObject("winmgmts:\\.\root\CIMV2")
	Set colItems      = objWMIService.ExecQuery("SELECT * FROM Win32_OperatingSystem WHERE Primary = TRUE", "WQL", &h10 + &h20)
	strVersion        = "5.0.0"
	For Each objItem In colItems
        On Error Resume Next
        strVersion    = objItem.Version
    Next
    Set objWMIService   = Nothing
    Set colItems        = Nothing
    getOSVersionBigNumber = CInt(Split(strVersion, ".")(0))
End Function

Function getOS
    'Caption=Microsoft Windows 7 专业版
    'CSDVersion=Service Pack 1
    'OSArchitecture=32-bit
    'Version=6.1.7601
	Dim objWMIService, colItems
	strReturnValue    = "{'Caption' : 'NONE', 'CSDVersion': 'NONE', 'CSName' : 'NONE', 'OSArchitecture': 'NONE', 'Version' : 'NONE'}"
    Set objWMIService = GetObject("winmgmts:\\.\root\CIMV2")
	Set colItems      = objWMIService.ExecQuery("SELECT * FROM Win32_OperatingSystem WHERE Primary = TRUE", "WQL", &h10 + &h20)
	For Each objItem In colItems
        On Error Resume Next
        strCaption          = "NONE"
        strCaption          = objItem.Caption
        strCSDVersion       = "NONE"
        strCSDVersion       = objItem.CSDVersion
        strCSName           = "NONE"
        strCSName           = objItem.CSName
        strOSArchitecture   = "NONE"
        strOSArchitecture   = objItem.OSArchitecture
        strVersion          = "NONE"
        strVersion          = objItem.Version
        strOSInfo           = "{'Caption'       : '{0}', 'CSDVersion': '{1}', 'CSName' : '{2}', " &_ 
                               "'OSArchitecture': '{3}', 'Version'   : '{4}'}"
        strReturnValue      = StrFormat(strOSInfo, Array(strCaption, strCSDVersion, strCSName, strOSArchitecture, strVersion))
    Next
    Set objWMIService   = Nothing
    Set colItems        = Nothing
    getOS               = strReturnValue
End Function

Function getCPU
		'Caption=x64 Family 6 Model 37 Stepping 5
		'DeviceID=CPU0
		'L2CacheSize=256
		'L3CacheSize=3072
		'Manufacturer=GenuineIntel
		'Name=Intel(R) Core(TM) i5 CPU       M 560  @ 2.67GHz
		'NumberOfCores=2
		'NumberOfLogicalProcessors=4
		'ProcessorId=BFEBFBFF00020655
		Dim objWMIService, colItems, CPUDict
		Set objWMIService = GetObject("winmgmts:\\.\root\CIMV2")
		Set colItems      = objWMIService.ExecQuery("SELECT * FROM Win32_Processor", "WQL",  &h10 + &h20)
		Set CPUDict       = CreateObject("Scripting.Dictionary")
		
		For Each objItem In colItems
			On Error Resume Next
			strDeviceID			= "NONE"
			strDeviceID   		= objItem.DeviceID
			strManufacturer 	= "NONE"
			strManufacturer 	= objItem.Manufacturer
			strName				= "NONE"
			strName				= objItem.Name
			strNumberOfCores 	= "1"
			strNumberOfCores 	= objItem.NumberOfCores
			strL2CacheSize      = "0"
			strL2CacheSize      = objItem.L2CacheSize
			strL3CacheSize      = "0"
			strL3CacheSize      = objItem.L3CacheSize
			
			strCPUInfo = "{'DeviceID'    : '{0}', 'Manufacturer' : '{1}' , 'Name' : '{2}', 'NumberOfCores' : '{3}'," &_
						 " 'L2CacheSize' : '{4}', 'L3CacheSize'  : '{5}'}"
			strCPUInfo = StrFormat(strCPUInfo, Array(strDeviceID, strManufacturer,strName, strNumberOfCores, strL2CacheSize, strL3CacheSize))
			CPUDict.Add strDeviceID, strCPUInfo
		Next
		
		strReturn = ""
		i = 0
		While i < CPUDict.Count
			If i = 0 Then
				strReturn =               "'" & i &"': " & CPUDict.Items()(i) & ""
			Else
				strReturn = strReturn & ", '" & i &"': " & CPUDict.Items()(i) & ""
			End If
			i = i + 1
		Wend
		
		Set objWMIService 	= Nothing
		Set colItems		= Nothing
		Set CPUDict			= Nothing
		getCPU = "{" & strReturn & "}"

End Function

Function getCPUUsage
    On Error Resume Next
    Dim objWMIService, colItems
	Set objWMIService = GetObject("winmgmts:\\.\root\CIMV2")
    Set colItems      = objWMIService.ExecQuery("Select * from Win32_Processor", "WQL",  &h10 + &h20)
    strReturnValue    = "{'cpuusage':'0'}"
    For Each objItem in colItems
        strLoadPercentage = "0"
        strLoadPercentage = objItem.LoadPercentage
        strReturnValue    = StrFormat("{'cpuusage' : '{0}'}", Array(strLoadPercentage))
    Next
    Set objWMIService = Nothing
    Set colItems      = Nothing
    getCPUUsage       = strReturnValue
End Function


Function getMemory()
    'FreePhysicalMemory=1675008
    'TotalVisibleMemorySize=3067564
	Dim objWMIService, colItems
    Set objWMIService = GetObject("winmgmts:\\.\root\CIMV2")
	Set colItems      = objWMIService.ExecQuery("SELECT * FROM Win32_OperatingSystem WHERE Primary = TRUE", "WQL", &h10 + &h20)
	strReturnValue    = "{'FreePhysicalMemory' : '1000', 'TotalVisibleMemorySize': '1000'}"
    For Each objItem In colItems
        On Error Resume Next
        strFreePhysicalMemory     = "999999999999"
        strFreePhysicalMemory     = objItem.FreePhysicalMemory
        strTotalVisibleMemorySize = "0"
        strTotalVisibleMemorySize = objItem.TotalVisibleMemorySize
        strMemInfo                = "{'FreePhysicalMemory' : '{0}', 'TotalVisibleMemorySize': '{1}'}"
        strReturnValue            = StrFormat(strMemInfo, Array(strFreePhysicalMemory, strTotalVisibleMemorySize))
    Next
    Set objWMIService   = Nothing
    Set colItems        = Nothing
    getMemory           = strReturnValue
End Function


Function getMemoryTopN(topNumber)
	''' 未完成TopN的排序功能
    Dim topN,objShell, objWshScriptExec, objStdOut, memTopNDict, memSortDict
    If TypeName(topN) <> "Error"  and IsNumeric(topNumber) Then
        topN = topNumber
    Else
        topN = 10
    End If
    strCmd               = "cmd.exe /c tasklist /V /FO CSV  /NH"
    Set objShell         = CreateObject("Wscript.Shell")
    Set objWshScriptExec = objShell.Exec(strCmd)
    Set objStdOut        = objWshScriptExec.StdOut
    Set memTopNDict      = CreateObject("Scripting.Dictionary")
    Set memSortDict		 = CreateObject("Scripting.Dictionary")
    
    ExitWhile            = False
    Count                = 0
    While (Not objStdOut.AtEndOfStream) And (Not ExitWhile)
        On Error Resume Next
        strLine   = objStdOut.ReadLine
        tempArray = Split(strLine, chr(34) & "," & chr(34))
        if ubound(tempArray) >8 Then
            Wscript.Echo strLine
        End If
        If Err.Number <> 0 or ubound(tempArray) <> 8Then
            Wscript.Echo Err.Description
            WScript.Echo  strLine
        Else
            strPID      = tempArray(1)
            strProcess  = Replace(tempArray(0), chr(34), "")
            strMemory   = Replace(Replace(tempArray(4), ",", ""), " K", "")
            strCPUTime  = tempArray(7)
            tempDictStr = "{'pid' : '{0}', 'process' : '{1}', 'memory' : '{2}', 'cputime':'{3}'}"
            tempDictStr = StrFormat(tempDictStr, Array(strPID, strProcess, strMemory, strCPUTime))
            memSortDict.Add strPID, CInt(strMemory)
            memTopNDict.Add strPID, tempDictStr
        End If
    Wend
    
End Function

Function getConnectionStatus
End Function


Function getNetworkListeningStatus
    Dim objShell, objWshScriptExec, objStdOut, StatusDict
    strCmd               = "cmd.exe /c netstat -ano | find " & chr(34) & "TCP" & chr(34) & " | find " & chr(34) & "LISTENING" & chr(34)
    Set objShell         = CreateObject("Wscript.Shell")
    Set objWshScriptExec = objShell.Exec(strCmd)
    Set objStdOut        = objWshScriptExec.StdOut
    Set StatusDict       = CreateObject("Scripting.Dictionary")
    ExitWhile            = False
    Count                = 0
    While (Not objStdOut.AtEndOfStream) And (Not ExitWhile)
        strLine   = objStdOut.ReadLine
        strLine   = rtrim(ltrim(strLine))
        Do Until InStr(strLine, "  ") = 0           ' Loop until there are no more double spaces
            strLine = Replace(strLine, "  ", " ")   ' Replace 2 spaces with 1 space
        Loop
        tempArray = Split(strLine, " ")             ' Now you can split on a single space
        
        If ubound(tempArray) = 4 Then
            strProtocol  = "NONE"
            strIPAndPort = "127.0.0.1:0"
            strIP        = "127.0.0.1"
            strPort      = "0"
            strPID       = "0"
            strProtocol  = tempArray(0)
            strIPAndPort = tempArray(1)
            tempArray2   = Split(strIPAndPort, ":")
            strPort      = tempArray2(UBound(tempArray2))
            strIP        = Replace(strIPAndPort, ":" & strPort, "")
            strPID       = tempArray(4)
            tempDictStr  = "{'Protocol' : '{0}', 'IP' : '{1}', 'Port':'{2}', 'PID': '{3}'}"
            tempDictStr  = StrFormat(tempDictStr, Array(strProtocol, strIP, strPort, strPID))
            Count        = Count + 1
            StatusDict.Add Count, tempDictStr
        End If
    Wend
    
    strReturn = ""
	i = 0
	While i < StatusDict.Count
			If i = 0 Then
				strReturn =               "'" & i &"': " & StatusDict.Items()(i) & ""
			Else
				strReturn = strReturn & ", '" & i &"': " & StatusDict.Items()(i) & ""
			End If
			i = i + 1
	Wend
    Set objShell              = Nothing
    Set objWshScriptExec      = Nothing
    Set objStdOut             = Nothing
    Set StatusDict            = Nothing
    getNetworkListeningStatus = "{" & strReturn & "}"       
End Function

Function getActiveMAC
    ''' 去活动网卡的MAC地址
    On Error Resume Next
    Set objWMIService   = GetObject("winmgmts:\\.\root\CIMV2")
    Set colItems        = objWMIService.ExecQuery("SELECT * FROM Win32_NetworkAdapterConfiguration WHERE IPEnabled=True", "WQL",  &h10 + &h20)
    Set activMACDict    = CreateObject("Scripting.Dictionary")
    For Each objItem In colItems
        ''''''''''''''''''''''''''''''先判断子网掩码有没有
        Dim boolSubnetExist
        boolSubnetExist = False
        For Each strSubnet In objItem.IPSubnet
            If Not IsNull(strSubnet) And Not IsEmpty(strSubnet) And Len(strSubnet) > 0 Then
                boolSubnetExist = True
            End If
        Next
        If boolSubnetExist Then
            activMACDict.Add objItem.MACAddress, objItem.MACAddress
        End If
	Next
    strReturn = ""
	i = 0
	While i < activMACDict.Count
		If i = 0 Then
			strReturn =               "'" & i &"' : '" & activMACDict.Items()(i) & "'"
		Else
			strReturn = strReturn & ", '" & i &"' : '" & activMACDict.Items()(i) & "'"
		End If
		i = i + 1
	Wend
    
    Set objWMIService = Nothing
    Set colItems      = Nothing
    Set activMACDict  = Nothing
    getActiveMAC      = "{" & strReturn & "}"
End Function

Function getCodeSet
    Dim objWMIService, colItems
    Set objWMIService   = GetObject("winmgmts:\\.\root\CIMV2")
	Set colItems        = objWMIService.ExecQuery("SELECT * FROM Win32_OperatingSystem WHERE Primary = TRUE", "WQL", &h10 + &h20)
	strReturnValue      = "936"
    
    For Each objItem In colItems
        On Error Resume Next
        strReturnValue  = objItem.CodeSet
    Next
    Set objWMIService   = Nothing
    Set colItems        = Nothing
    getCodeSet          = strReturnValue
End Function

Function GetSysInfo
    ''' 组合系统信息Dict
    Dim SysInfoDict
	Set SysInfoDict = CreateObject("Scripting.Dictionary")
	SysInfoDict.Add "os",               StrFormat("'{0}' : {1}", Array( "os",               getOS()))
	SysInfoDict.Add "uptime",           StrFormat("'{0}' : {1}", Array( "uptime",           getUpTime()))
	SysInfoDict.Add "codeset",          StrFormat("'{0}' : {1}", Array( "codeset",          getCodeSet()))
	SysInfoDict.Add "cpu",              StrFormat("'{0}' : {1}", Array( "cpu",              getCPU()))
	SysInfoDict.Add "cpuusage",         StrFormat("'{0}' : {1}", Array( "cpuusage",         getCPUUsage()))
	SysInfoDict.Add "networklistening", StrFormat("'{0}' : {1}", Array( "networklistening", getNetworkListeningStatus()))
	SysInfoDict.Add "disk",             StrFormat("'{0}' : {1}", Array( "disk",             getDisk()))
	SysInfoDict.Add "memory",           StrFormat("'{0}' : {1}", Array( "memory",           getMemory()))
	SysInfoDict.Add "activemac",        StrFormat("'{0}' : {1}", Array( "activemac",        getActiveMAC()))
	'SysInfoDict.Add "unused",           "{'unused' : 'unused'}"
	
    GetSysInfo      = "{" & Join(SysInfoDict.Items(), ",") & "}"
    Set SysInfoDict = Nothing
End Function

Function getHTTPContent(strUrl, strPostData)
    On Error Resume Next
    strReturnValue = ""
    Dim xmlhttp
    Set xmlhttp    = CreateObject("MSXML2.ServerXMLHTTP")
    'resolveTimeout,connectTimeout,sendTimeout,receiveTimeout
    With xmlhttp
        .SetTimeouts CONFIG_RESOLVE_TIMEOUT, CONFIG_CONNECT_TIMEOUT, CONFIG_SEND_TIMEOUT, CONFIG_RECEIVE_TIMEOUT
        .SetOption 2, 13056             ' 忽略服务端证书校验
        .Open "POST", strUrl, False
        .SetRequestHeader "Content-Type", "application/x-www-form-urlencoded; charset=utf-8"
        '.SetRequestHeader "Content-Length", Len(postStr)
        .Send strPostData
        xmlHttpResponse = .responseText
        If Err.Number = 0 And .status = 200 Then
            strReturnValue = xmlHttpResponse
        Else
            If Err.Number <> 0  Then
        	    WScript.Echo "Exception:"                                & vbCrLf &_
                         	 "    Error number     : " & Err.Number      & vbCrLf &_
                         	 "    Error description: " & Err.Description & vbCrLf
    	    End If
    	End If
    End With
    Set xmlhttp    = Nothing
    getHTTPContent = strReturnValue
End Function

Function getPostData
    ''' 组合POST数据包
    PostStr     = "CLIENTTYPE="        & CONFIG_CLIENTTYPE      &_
                  "&CLIENTUID="        & CONFIG_CLIENTUID       &_
                  "&VERSION="          & CONFIG_RELEASE_VERSION &_
                  "&CONFIG_USERID="    & CONFIG_USERID          &_
                  "&CONFIG_APPSECKEY=" & CONFIG_APPSECKEY       &_
                  "&CODESET="          & getCodeSet()           &_
                  "&SYSINFO="          & EncodeURIComponent(GetSysInfo())
    getPostData = PostStr
End Function

Function EncodeURIComponent(str)
    ''' URL Encode copyed from Internet
    Dim i,c,ic,s,length
    length = Len(str)
    For i = 1 To length
        s = Mid(str,i,1)
        c = "&H" & Hex(AscW(Mid(str,i,1)))
        ic = CInt(c)
    
        If ( ic >= AscW("A") And ic <= AscW("Z") ) Or _
          ( ic >= AscW("a") And ic <= AscW("z") ) Or _
          ( ic >= AscW("0") And ic <= AscW("9") ) Or _
          ( ic = AscW("-") Or ic = AscW("_") Or ic = AscW(".")  Or ic = AscW("!")  Or ic = AscW("~") Or ic = AscW("*") Or ic = AscW("'") Or ic = AscW("(") Or ic=AscW(")") ) Then
          EncodeURIComponent = EncodeURIComponent & s
        ElseIf ic > 16 And ic < 128 Then
          EncodeURIComponent = EncodeURIComponent & "%" & Hex(ic)
        Else
          If c >= &H0001 And c <= &H007F Then
            EncodeURIComponent = EncodeURIComponent & s
          ElseIf c > &H07FF Then
            EncodeURIComponent = EncodeURIComponent & "%" & Hex(&HE0 Or (c\(2^12) And &H0F))
            EncodeURIComponent = EncodeURIComponent & "%" & Hex(&H80 Or (c\(2^6) And &H3F))
            EncodeURIComponent = EncodeURIComponent & "%" & Hex(&H80 Or (c\(2^0) And &H3F))
          Else
            EncodeURIComponent = EncodeURIComponent & "%" & Hex(&HC0 Or (c\(2^6) And &H1F))
            EncodeURIComponent = EncodeURIComponent & "%" & Hex(&H80 Or (c\(2^0) And &H3F))
          End If
        End If
    Next
End Function

Function getLogFilePath
    ''' 返回日志文件路径 
	getLogFilePath = Replace(WScript.ScriptFullName, WScript.ScriptName, "yibanfu.log")
End Function

Function WriteLog(Str)
    ''' 写日志 
	On Error  Resume Next
	Set objFSO  = CreateObject("Scripting.FileSystemObject")
	Set objFile = objFSO.OpenTextFile(getLogFilePath(), 8, True)
    objFile.WriteLine Now & ":" & Str
    objFile.Close
    Set objFile = Nothing
    Set objFSO  = Nothing
End Function